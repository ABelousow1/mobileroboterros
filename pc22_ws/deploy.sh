#!/bin/bash -i

robot_user=prac2022
ROS_MASTER_URI="http://leo:11311"

workspace=$(roscd; cd ..; pwd)/src/

if [[ "$workspace" != ${HOME}/* ]]
then
	echo "You forgot to source your local workspace!"
	echo "The first workspace found is '$workspace'"
	exit 1
fi

robot=$(echo $ROS_MASTER_URI | sed 's@http://\(.*\):11311@\1@')

if [[ -z "$robot" || "$robot" == localhost ]]
then
	echo "You did not export a robot's ROS_MASTER_URI."
	echo "Current ROS_MASTER_URI is '$ROS_MASTER_URI'."
	exit 1
fi

rsync -ar "$workspace" $robot_user@$robot:ros/src/prac && 
ssh $robot_user@$robot -t "bash -i -c 'roscd && cd .. && catkin build'"

