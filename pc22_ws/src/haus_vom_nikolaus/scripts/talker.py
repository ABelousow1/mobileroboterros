#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

## Simple talker demo that published std_msgs/Strings messages
## to the 'chatter' topic

import rospy
from math import sqrt
from geometry_msgs.msg import Twist

#!/usr/bin/env python
from std_msgs.msg import String


    
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("talker", String, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

def move2():
    pub = rospy.Publisher('/cmd_vel_mux/input/navi', Twist, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rospy.sleep(1)
    vel_msg = Twist()
    speed = 0.15
    current_distance = 0
    t0 = rospy.Time.now().to_sec()
    distance = 1

    while(current_distance < distance):
        vel_msg.linear.y = speed
        vel_msg.linear.x = speed
        pub.publish(vel_msg)
        t1=rospy.Time.now().to_sec()
        current_distance = speed*(t1-t0)
        

def move():
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    vel_msg = Twist()
    rospy.sleep(2)
    x = 6
    line_vel = 2
    rate = rospy.Rate(0.3)
    PI = 3.14159
    
    msg_go = Twist()
    msg_go.linear.x = line_vel
    msg_go.linear.y = 0


    pub.publish(msg_go)
    rate.sleep()
    
    msg_go.linear.x=0
    msg_go.linear.y=line_vel
    
    pub.publish(msg_go)
    rate.sleep()
    
    msg_go.linear.x=-line_vel
    msg_go.linear.y=0
    pub.publish(msg_go)
    rate.sleep()
    
    msg_go.linear.x=0
    msg_go.linear.y=-line_vel
   
    pub.publish(msg_go)
    rate.sleep()
    # msg_go.linear.x=-line_vel
    # msg_go.linear.y=0
    # rate.sleep()
    # pub.publish(msg_go)

    msg_go.linear.x= line_vel
    msg_go.linear.y= line_vel 
    pub.publish(msg_go)
    rate.sleep()

    msg_go.linear.x= -line_vel/2
    msg_go.linear.y= line_vel/2 
    pub.publish(msg_go)
    rate.sleep()

    msg_go.linear.x= -line_vel/2
    msg_go.linear.y= -line_vel/2 
    pub.publish(msg_go)
    rate.sleep()
    
    msg_go.linear.x= line_vel
    msg_go.linear.y= -line_vel 
    pub.publish(msg_go)
    rate.sleep()
    


if __name__ == '__main__':
    try:
        move2()
    except rospy.ROSInterruptException:
        pass
